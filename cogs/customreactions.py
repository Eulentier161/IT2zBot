import discord
from discord.ext import commands
from util.db import Database
from util.util import Utils
from sqlalchemy.exc import IntegrityError


class CustomReactionsCog(commands.Cog):
    """commands to manage custom reactions"""

    def __init__(self, bot):
        self.bot = bot
        self.db = Database()

    @commands.command(aliases=["acr"])
    async def add_custom_reaction(self, ctx, trigger, *, response):
        """add a custom reaction to the bot"""
        try:
            self.db.create_custom_reaction(trigger, response, ctx.author.id)
            reac, resp = "✅", response
        except IntegrityError:
            reac, resp = "❌", f"custom reaction with {trigger=} already exists"
        finally:
            await ctx.message.add_reaction(reac)
            await ctx.reply(resp)

    @commands.Cog.listener("on_message")
    async def custom_reaction_listener(self, message):
        if message.author == self.bot.user:
            return
        res = self.db.get_custom_reaction(message.content)
        if not res:
            return
        await message.channel.send(res)

    @commands.command(aliases=["dcr"])
    async def delete_custom_reaction(self, ctx, trigger):
        await ctx.message.add_reaction("✅" if self.db.delete_custom_reaction(trigger, ctx.author.id) else "❌")
